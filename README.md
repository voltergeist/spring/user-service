# User Service

API responsável pela gestão do usuário

## Começando

A única dependência externa é o mongodb que pode tanto ser baixado por [aqui](https://www.mongodb.com/try/download/community) quanto usando a [imagem docker](https://hub.docker.com/_/mongo)

**Se você já possui o mongodb verifique o usuário e senha!**

## Endpoints

Para ter acesso a lista completa basta acessar o link ```http://localhost:8080/swagger-ui/index.html``` com o servidor em execução

## Pontos de Atenção
Como visto na Annotation ```@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })``` por hora as autenticações do Spring Security estão desativada. Em breve reativarei e farei as devidas validações.
