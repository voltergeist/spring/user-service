package br.com.voltergeist.userservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.voltergeist.userservice.service.LoginService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/user-service/user")
@AllArgsConstructor
public class UserController {

	private final LoginService service;

	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestParam String username, @RequestParam String password) {
		return service.loginAuthentication(username, password);
	}
}
