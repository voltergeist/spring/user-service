package br.com.voltergeist.userservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.voltergeist.userservice.model.LoginModel;

public interface LoginRepository extends MongoRepository<LoginModel, String> {

	Optional<LoginModel> findByUsername(String username);

}
