package br.com.voltergeist.userservice.service;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.voltergeist.userservice.model.LoginModel;
import br.com.voltergeist.userservice.repository.LoginRepository;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class LoginServiceImpl implements LoginService {

	private final BCryptPasswordEncoder encoder;
	private final LoginRepository repository;

	@Override
	public ResponseEntity<String> loginAuthentication(String username, String password) {
		
		LoginModel login = findByUsername(username).getBody();
		
		if(isInvalidLogin(password, login)) {
			return new ResponseEntity<String>("Usuário ou senha inválido!", HttpStatus.UNAUTHORIZED);
		}
		
		return ResponseEntity.ok(username);
	}

	private boolean isInvalidLogin(String password, LoginModel login) {
		return login == null || !passwordsMatch(password, login);
	}

	private boolean passwordsMatch(String password, LoginModel login) {
		return encoder.matches(password, login.getPassword());
	}

	@Override
	public ResponseEntity<LoginModel> findByUsername(String username) {
		Optional<LoginModel> login = repository.findByUsername(username);
		
		if (login.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(login.get());
	}
}
