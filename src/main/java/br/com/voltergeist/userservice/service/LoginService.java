package br.com.voltergeist.userservice.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.voltergeist.userservice.model.LoginModel;

@Service
public interface LoginService {

	ResponseEntity<String> loginAuthentication(String username, String password);
	
	ResponseEntity<LoginModel> findByUsername(String username);

}
