package br.com.voltergeist.userservice.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("user")
public class LoginModel {
	
	@Id
	private String id;
	
	@NotBlank(message = "Usuário não pode estar vazio")
	@NotNull(message = "Usuário não pode estar vazio")
	private String username;
	
	@NotBlank(message = "Senha não pode estar vazia")
	@NotNull(message = "Senha não pode estar vazia")
	private String password;
}
