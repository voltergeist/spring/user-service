package br.com.voltergeist.userservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.voltergeist.userservice.model.LoginModel;
import br.com.voltergeist.userservice.repository.LoginRepository;

@SpringBootTest
@AutoConfigureDataMongo
public class LoginServiceImplTest {

	@Autowired
	private LoginService service;

	@MockBean
	private LoginRepository repositoryMock;

	@MockBean
	private BCryptPasswordEncoder encoder;

	private String username;
	private String password;

	private LoginModel login;

	@BeforeEach
	void resetVariables() {
		username = "username";
		password = "password";
		login = new LoginModel(null, username, password);
	}

	@Test
	void mustReturnInvalidUsernameOrPassword() {

		when(repositoryMock.findByUsername(username)).thenReturn(Optional.of(login));
		when(encoder.matches(anyString(), anyString())).thenReturn(false);

		assertEquals(service.loginAuthentication(username, password).getBody(), "Usuário ou senha inválido!");
	}

	@Test
	void mustReturn401WithInvalidUsernameOrPassword() {

		when(repositoryMock.findByUsername(username)).thenReturn(Optional.empty());

		assertEquals(service.loginAuthentication(username, password).getStatusCode(), HttpStatus.UNAUTHORIZED);
	}

	@Test
	void mustReturn200WithValidUsernameOrPassword() {

		when(repositoryMock.findByUsername(username)).thenReturn(Optional.of(login));
		when(encoder.matches(anyString(), anyString())).thenReturn(true);

		assertEquals(service.loginAuthentication(username, password).getStatusCode(), HttpStatus.OK);
	}
}
