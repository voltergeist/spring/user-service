package br.com.voltergeist.userservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.voltergeist.userservice.model.LoginModel;
import br.com.voltergeist.userservice.repository.LoginRepository;

@SpringBootTest
public class UserControllerTest {
	
	@Autowired
	private UserController controller;
	
	@MockBean
	private LoginRepository repositoryMock;
	
	@MockBean
	private BCryptPasswordEncoder encoder;

	
	private String username;
	private String password;
	private LoginModel login;
	
	@BeforeEach
	void resetVariables() {
		username = "username";
		password = "password";
		login = new LoginModel(null, username, password);
	}
	
	@Test
	public void mustReturn200() {
		when(repositoryMock.findByUsername(username)).thenReturn(Optional.of(login));
		when(encoder.matches(anyString(), anyString())).thenReturn(true);
		
		assertEquals(controller.login("username", "password").getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void mustReturnUsername() {
		when(repositoryMock.findByUsername(username)).thenReturn(Optional.of(login));
		when(encoder.matches(anyString(), anyString())).thenReturn(true);
		
		assertEquals(controller.login("username", "password").getBody(), username);
	}
	
	@Test
	public void mustReturn401() {
		when(repositoryMock.findByUsername(username)).thenReturn(Optional.empty());
		
		assertEquals(controller.login("username", "password").getStatusCode(), HttpStatus.UNAUTHORIZED);
	}
}
